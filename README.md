# flat-remix

Flat Remix is an icon theme inspired by material design. It is mostly flat using a colorful palette with some shadows, highlights, and gradients for some depth.

https://drasite.com/flat-remix

https://github.com/daniruiz/Flat-Remix

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/flat-remix.git
```
